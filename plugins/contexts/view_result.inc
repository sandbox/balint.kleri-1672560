<?php

/**
 * @file view_result.inc
 * Plugin to provide a View result context.
 */

$plugin = array(
  'title' => t('View result'),
  'description' => t('Result of a view.'),
  'context' => 'view_result_context_create_view_result',
  'keyword' => 'view-result',
  'context name' => 'view_result',
  'convert list' => 'view_result_context_view_result_convert_list',
  'convert' => 'view_result_context_view_result_convert',
);

function view_result_context_create_view_result($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('view_result');
  $context->plugin = 'view_result';

  if ($empty) {
    return $context;
  }

  if (!empty($data) && !empty($data->view_result)) {
    $context->data = $data->view_result;
    $context->title = $data->title;
    $context->argument = $data->argument;
    $context->view_base_field = $data->view_base_field;
    return $context;
  }
}

function view_result_context_view_result_convert_list() {
  return array(
    'identifiers_and' => t('Identifiers listed with "+" sign in between them, e.g.: 1+2+3.'),
    'identifiers_or' => t('Identifiers listed with "," sign in between them, e.g.: 1,2,3.'),
  );
}

function view_result_context_view_result_convert($context, $type) {
  $result = array();
  foreach ($context->data as $row) {
    $result[] = $row->{$context->view_base_field};
  }
  switch ($type) {
    case 'identifiers_and':
      $glue = '+';
      break;
    case 'identifiers_or':
      $glue = ',';
      break;
  }
  return implode($glue, $result);
}

