<?php

/**
 * @file view_result.inc
 * Plugin to provide a relationship handler for extracting the result of a view.
 */

$plugin = array(
  'title' => t('Result from view'),
  'keyword' => 'view-result',
  'description' => t('Extract a View result context from a view.'),
  'required context' => new ctools_context_required(t('View'), 'view'),
  'context' => 'view_result_result_from_view_context',
  'defaults' => array(),
);

/**
 * Creates a View result context from the View context.
 */
function view_result_result_from_view_context($context, $conf, $placeholder = FALSE) {
  if (empty($context->data) || $placeholder) {
    return ctools_context_create_empty('view_result', NULL);
  }
  $view = views_content_context_get_view($context);
  // Ensure the view executes, but we don't need its output.
  views_content_context_get_output($context);

  if (!empty($view->result)) {
      $data = new stdClass();
      $data->view_result = $view->result;
      $data->view_base_field = $view->base_field;
      $data->title = $view->get_title();
      $data->argument = $view->name . ':' . $view->current_display;
      return ctools_context_create('view_result', $data);
  }
  return ctools_context_create_empty('view_result', NULL);
}

